#!/usr/bin/python
# -*- coding: utf-8 -*-
#Marko Mandel
#
#Skript loeb sisendfailist Skriptimiskeelte ainesse registreerunud isikute nimed ja loob I-Teele #sobiva väljundfaili (CSV).
#
import sys
import os
import string
import random 


#kontrollib kas argumente on 2
if len(sys.argv) != 3:
    print 'Kasutamine: generaator.py <sisend> <v2ljund>'
    sys.exit()

sisendfail = os.path.abspath(sys.argv[1])
v2ljundfail = os.path.abspath(sys.argv[2])

#kontrolli kas sisendfail on olemas
try:
    open(sisendfail)
except IOError:
    print 'Viga: Sisendfaili ei leitud!'
    sys.exit()
else:
    pass


#paneb sisendfaili 2D-massiivi
sisend = open(sisendfail)    
url = []
for line in sisend.readlines():
    line = line.strip()
    if line == '':
        continue
    url.append(line.split(' '))   
sisend.close()


#print url
#massiivi puhastus:
sisendandmed = []
rida1 = []
for line in url:
    rida1 = filter(None, line)
   # i = line.index('\t')
    #rida1 = rida1[i:]
    sisendandmed.append(rida1)
sisendandmed.pop(0)    

#loob vajalikud andmed ja kirjutab need 6ige vorminguga massiivi
v2ljund = []
for line in sisendandmed:
    try:
        line[1]
        line[2]
        line[3]
    except IndexError:
        print 'Viga: Sisendfail ei vasta n6uetele!'
        sys.exit()
    else:
        pass
    eesnimi = line[1]
    perenimi = line[2]
    ryhm = line[3]
    kasutajatunnus = eesnimi[0].lower() + perenimi.lower() 
    kasutajatunnus = (kasutajatunnus[:8]) if len(kasutajatunnus) > 8 else kasutajatunnus
    nimi = eesnimi + ' ' + perenimi
    epost = eesnimi.lower() + '.' + perenimi.lower() + '@itcollege.ee'
    symbolid = ''.join((string.lowercase, string.uppercase, string.digits, "_"))
    token =''.join(random.choice(symbolid) for sym in range(20))
    token = ryhm + token
    token = (token[:20]) if len(token) > 20 else token
    #massiivi kirjutamine
    v2ljund.append(kasutajatunnus + ',' + nimi + ',' + epost + ',' + token) 

    
    
#loob v2ljundifaili, kui seda veel ei ole. Kirjutab tulemused faili. Kui fail on olemas, siis kirjuta yle
file = open(v2ljundfail, "w")
for rida in v2ljund:
    file.write(rida + '\n')
file.close()

print '\nValmis! V2ljund on failis ' + v2ljundfail




























